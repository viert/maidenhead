use crate::lang::lexer::Token;

#[derive(Debug, Clone)]
pub struct Identifier {
  pub token: Token,
  pub name: String,
}

#[derive(Debug, Clone)]
pub enum Literal {
  Integer(u64, Token),
  Float(f64, Token),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Operator {
  Plus(Token),
  Minus(Token),
  Divide(Token),
  Multiply(Token),
  And(Token),
  Or(Token),
  Not(Token),
  Equal(Token),
  NotEqual(Token),
  Less(Token),
  LessOrEqual(Token),
  Greater(Token),
  GreaterOrEqual(Token),
}

impl Operator {
  fn precedence(&self) -> u8 {
    match self {
      Operator::Plus(_) => 11,
      Operator::Minus(_) => 11,
      Operator::Divide(_) => 12,
      Operator::Multiply(_) => 12,
      Operator::And(_) => 4,
      Operator::Or(_) => 3,
      Operator::Equal(_) => 8,
      Operator::NotEqual(_) => 8,
      Operator::Less(_) => 9,
      Operator::LessOrEqual(_) => 9,
      Operator::Greater(_) => 9,
      Operator::GreaterOrEqual(_) => 9,
      Operator::Not(_) => 14,
    }
  }
}

impl PartialOrd for Operator {
  fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
    self.precedence().partial_cmp(&other.precedence())
  }
}
