use super::basics::{Identifier, Literal, Operator};

#[derive(Debug, Clone)]
pub struct Expression {
  pub left: Box<LeftExpression>,
  pub op: Option<Operator>,
  pub right: Option<Box<Expression>>,
}

#[derive(Debug, Clone)]
pub enum LeftExpression {
  Ident(Identifier),
  Literal(Literal),
  Expr(Expression),
  FuncCall(Identifier, Vec<Expression>),
}

impl From<LeftExpression> for Expression {
  fn from(value: LeftExpression) -> Self {
    match value {
      LeftExpression::Ident(_) | LeftExpression::Literal(_) | LeftExpression::FuncCall(_, _) => {
        Self {
          left: Box::new(value),
          op: None,
          right: None,
        }
      }
      LeftExpression::Expr(expr) => expr,
    }
  }
}
