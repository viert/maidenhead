use maidenhead::{
  lang::{compiler::Compiler, lexer::Lexer, parser::Parser},
  vm::Interpreter,
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
  let src = r#"
  fn add(a, b) {
    ret a + b;
  }

  let c = add(5, 12);
  "#;

  let lexer = Lexer::new(src);
  let tc = lexer.parse()?;

  let mut parser = Parser::new(tc);
  let ast = parser.parse(src)?;

  let mut compiler = Compiler::new(src);
  let prog = compiler.compile(ast)?;
  prog.dump();

  let mut vm = Interpreter::new(prog, 32, 1);
  vm.run()?;

  // println!("{vm:#?}");
  Ok(())
}
