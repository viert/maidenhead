use super::source::{Source, Span};
use enum_display_derive::Display as EnumDisplay;
use lazy_static::lazy_static;
use regex::Regex;
use std::{
  collections::{HashMap, HashSet},
  error::Error,
  fmt::Display,
  str::FromStr,
};

lazy_static! {
  static ref WHITESPACE: Regex = Regex::from_str(r"\s").unwrap();
  static ref IDENT_START: Regex = Regex::from_str(r"[a-zA-Z_]").unwrap();
  static ref IDENT_NEXT: Regex = Regex::from_str(r"[a-zA-Z0-9_]").unwrap();
  static ref KEYWORDS: HashSet<&'static str> = {
    let keywords = vec!["let", "if", "while", "fn", "ret"];
    HashSet::from_iter(keywords)
  };
  static ref SINGLE_CHAR_TOKENS: HashMap<char, TokenType> = {
    let mut map = HashMap::new();
    map.insert('(', TokenType::LeftParenthesis);
    map.insert(')', TokenType::RightParenthesis);
    map.insert('{', TokenType::LeftBrace);
    map.insert('}', TokenType::RightBrace);
    map.insert('+', TokenType::Plus);
    map.insert('-', TokenType::Minus);
    map.insert('/', TokenType::Slash);
    map.insert('*', TokenType::Asterisk);
    map.insert(';', TokenType::Semicolon);
    map.insert(',', TokenType::Comma);
    map
  };
}

#[derive(Debug, Clone, PartialEq, EnumDisplay)]
pub enum TokenType {
  EOF,
  Identifier,
  Keyword,
  Integer,
  Float,

  LeftParenthesis,
  RightParenthesis,
  LeftBrace,
  RightBrace,

  And,
  Or,

  Plus,
  Minus,
  Slash,
  Asterisk,
  Exclamation,
  Semicolon,
  Comma,

  Assign,
  Equal,
  NotEqual,
  Less,
  LessOrEqual,
  Greater,
  GreaterOrEqual,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Token {
  pub token_type: TokenType,
  pub line: usize,
  pub pos: usize,
  pub span: Span,
}

impl Token {
  pub fn literal<'a>(&self, src: &'a str) -> &'a str {
    &src[self.span.0..self.span.1]
  }
}

pub struct TokenCursor {
  tokens: Vec<Token>,
  curr: Option<Token>,
  next: Option<Token>,
}

impl TokenCursor {
  pub fn new(tokens: Vec<Token>) -> Self {
    let mut tokens: Vec<Token> = tokens.into_iter().rev().collect();
    let curr = tokens.pop();
    let next = tokens.pop();
    Self { tokens, curr, next }
  }

  pub fn curr(&self) -> Option<&Token> {
    self.curr.as_ref()
  }

  pub fn ahead(&self) -> Option<&Token> {
    self.next.as_ref()
  }

  pub fn advance(&mut self) {
    if self.curr.is_some() {
      self.curr = self.next.take();
      self.next = self.tokens.pop();
    }
  }
}

#[derive(Debug)]
pub struct LexerError {
  msg: String,
}

impl Display for LexerError {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "Parse error: {}", self.msg)
  }
}

impl Error for LexerError {}

impl LexerError {
  pub fn new<T: AsRef<str> + Display>(msg: T, line: usize, pos: usize) -> Self {
    Self {
      msg: format!("{} at line {line} pos {pos}", msg),
    }
  }
}

pub struct Lexer<'a> {
  src: Source<'a>,
  tokens: Vec<Token>,
}

impl<'a> Lexer<'a> {
  pub fn new(src: &'a str) -> Self {
    let src = Source::new(src);
    Self {
      src,
      tokens: Default::default(),
    }
  }

  fn parse_number(&mut self) -> Result<(), LexerError> {
    let mut dot_met = false;
    let line = self.src.line();
    let pos = self.src.pos();
    let idx = self.src.idx();

    loop {
      let ch = self.src.curr();
      if let Some(ch) = ch {
        match ch {
          "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" => {
            self.src.advance();
          }
          "." => {
            if !dot_met {
              dot_met = true;
              self.src.advance();
            } else {
              return Err(LexerError::new(
                "invalid float: unexpected \".\"",
                self.src.line(),
                self.src.pos(),
              ));
            }
          }
          _ => {
            break;
          }
        }
      } else {
        break;
      }
    }
    let span = Span(idx, self.src.idx());
    let token_type = if dot_met {
      TokenType::Float
    } else {
      TokenType::Integer
    };
    let token = Token {
      token_type,
      line,
      pos,
      span,
    };
    self.tokens.push(token);
    Ok(())
  }

  pub fn parse_ident(&mut self) -> Result<(), LexerError> {
    let line = self.src.line();
    let pos = self.src.pos();
    let idx = self.src.idx();

    let mut acc = String::new();
    loop {
      let ch = self.src.curr();
      if let Some(ch) = ch {
        if !IDENT_NEXT.is_match(ch) {
          break;
        }
        acc.push_str(ch);
      } else {
        break;
      }
      self.src.advance();
    }

    let span = Span(idx, self.src.idx());
    let token_type = if KEYWORDS.contains(acc.as_str()) {
      TokenType::Keyword
    } else {
      TokenType::Identifier
    };
    let token = Token {
      token_type,
      line,
      pos,
      span,
    };
    self.tokens.push(token);
    Ok(())
  }

  fn parse_single_char(&mut self) -> Result<(), LexerError> {
    let ch = self.src.curr().unwrap().chars().next().unwrap();
    let token_type = SINGLE_CHAR_TOKENS.get(&ch).unwrap().clone();
    let token = Token {
      token_type,
      line: self.src.line(),
      pos: self.src.pos(),
      span: Span(self.src.idx(), self.src.idx() + 1),
    };
    self.tokens.push(token);
    self.src.advance();
    Ok(())
  }

  fn parse_followed_by_equals(
    &mut self,
    followed: TokenType,
    not_followed: TokenType,
  ) -> Result<(), LexerError> {
    let line = self.src.line();
    let pos = self.src.pos();
    let idx = self.src.idx();
    self.src.advance();
    let ch = self.src.curr();
    let token_type = if let Some(ch) = ch {
      if ch == "=" {
        self.src.advance();
        followed
      } else {
        not_followed
      }
    } else {
      not_followed
    };

    let span = Span(idx, self.src.idx());
    let token = Token {
      token_type,
      line,
      pos,
      span,
    };
    self.tokens.push(token);
    Ok(())
  }

  pub fn parse(mut self) -> Result<TokenCursor, LexerError> {
    loop {
      let ch = self.src.curr();
      if let Some(ch) = ch {
        if WHITESPACE.is_match(ch) {
          self.src.advance();
          continue;
        }

        if IDENT_START.is_match(ch) {
          self.parse_ident()?;
          continue;
        }

        let sch = ch.chars().next();
        if let Some(sch) = sch {
          if SINGLE_CHAR_TOKENS.contains_key(&sch) {
            self.parse_single_char()?;
            continue;
          }
        }

        match ch {
          "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" => self.parse_number()?,
          "=" => self.parse_followed_by_equals(TokenType::Equal, TokenType::Assign)?,
          "!" => self.parse_followed_by_equals(TokenType::NotEqual, TokenType::Exclamation)?,
          "<" => self.parse_followed_by_equals(TokenType::LessOrEqual, TokenType::Less)?,
          ">" => self.parse_followed_by_equals(TokenType::GreaterOrEqual, TokenType::Greater)?,
          "&" => {
            let line = self.src.line();
            let pos = self.src.pos();
            let idx = self.src.idx();
            self.src.advance();
            let ch = self.src.curr();
            if let Some(ch) = ch {
              if ch == "&" {
                self.src.advance();
                let span = Span(idx, self.src.idx());
                self.tokens.push(Token {
                  token_type: TokenType::And,
                  line,
                  pos,
                  span,
                });
                continue;
              }
            }
            return Err(LexerError::new("unexpected character &", line, pos));
          }
          "|" => {
            let line = self.src.line();
            let pos = self.src.pos();
            let idx = self.src.idx();
            self.src.advance();
            let ch = self.src.curr();
            if let Some(ch) = ch {
              if ch == "|" {
                self.src.advance();
                let span = Span(idx, self.src.idx());
                self.tokens.push(Token {
                  token_type: TokenType::Or,
                  line,
                  pos,
                  span,
                });
                continue;
              }
            }
            return Err(LexerError::new("unexpected character |", line, pos));
          }
          _ => {
            return Err(LexerError::new(
              format!("unexpected character \"{ch}\""),
              self.src.line(),
              self.src.pos(),
            ))
          }
        }
      } else {
        let token = Token {
          token_type: TokenType::EOF,
          line: self.src.line(),
          pos: self.src.pos(),
          span: Span(self.src.idx(), self.src.idx()),
        };
        self.tokens.push(token);
        break;
      }
    }
    Ok(TokenCursor::new(self.tokens))
  }
}
