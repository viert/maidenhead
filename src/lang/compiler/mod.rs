use std::{collections::HashMap, error::Error, fmt::Display};

use crate::vm::Instruction;

use super::{
  lexer::Token,
  parser::{
    basics::{Identifier, Literal, Operator},
    expression::{Expression, LeftExpression},
    statement::{FuncStatement, LetStatement, RetStatement, Statement, AST},
  },
};

#[derive(Debug)]
pub struct Program {
  pub code: Vec<Instruction>,
}

impl Program {
  pub fn dump(&self) {
    for (ip, instr) in self.code.iter().enumerate() {
      println!("{ip:04}: {instr}")
    }
  }
}

enum CtxVar {
  Local(usize),
  Global(usize),
  Arg(usize),
}

#[derive(Default, Debug, Clone)]
struct Context {
  locals: HashMap<String, usize>,
  funcs: HashMap<String, usize>,
  args: HashMap<String, usize>,
  parent: Option<Box<Context>>,
}

impl Context {
  pub fn new(parent: Option<&Context>) -> Self {
    Self {
      locals: Default::default(),
      funcs: Default::default(),
      args: Default::default(),
      parent: parent.map(|ctx| Box::new(ctx.clone())),
    }
  }

  pub fn is_global(&self) -> bool {
    self.parent.is_none()
  }

  fn ctxvar(&self, id: usize) -> CtxVar {
    if self.is_global() {
      CtxVar::Global(id)
    } else {
      CtxVar::Local(id)
    }
  }

  pub fn get_func(&self, name: &str) -> Option<usize> {
    let addr = self.funcs.get(name);
    if let Some(addr) = addr {
      Some(*addr)
    } else if let Some(parent) = &self.parent {
      parent.get_func(name)
    } else {
      None
    }
  }

  pub fn ensure_func_ident(&self, ident: &Identifier) -> Result<usize, CompileError> {
    self.get_func(&ident.name).ok_or(CompileError::new(
      &format!("function {} is undefined", ident.name),
      ident.token.clone(),
    ))
  }

  pub fn set_func(&mut self, ident: &Identifier, ip: usize) -> Result<(), CompileError> {
    if self.funcs.contains_key(&ident.name) {
      Err(CompileError::new(
        &format!("function {} is already defined", ident.name),
        ident.token.clone(),
      ))
    } else {
      self.funcs.insert(ident.name.to_owned(), ip);
      Ok(())
    }
  }

  pub fn get(&self, name: &str) -> Option<CtxVar> {
    let id = self.locals.get(name).copied();
    if let Some(id) = id {
      Some(self.ctxvar(id))
    } else {
      let id = self.args.get(name).copied();
      if let Some(id) = id {
        Some(CtxVar::Arg(id))
      } else if let Some(parent) = &self.parent {
        parent.get(name)
      } else {
        None
      }
    }
  }

  pub fn ensure(&self, name: &str, token: Token) -> Result<CtxVar, CompileError> {
    self.get(name).ok_or(CompileError::new(
      &format!("variable {} is undefined", name),
      token,
    ))
  }

  pub fn ensure_ident(&self, ident: &Identifier) -> Result<CtxVar, CompileError> {
    self.ensure(&ident.name, ident.token.clone())
  }

  pub fn set(&mut self, name: &str, token: Token) -> Result<CtxVar, CompileError> {
    if self.locals.contains_key(name) {
      Err(CompileError::new(
        &format!("variable {} is already defined", name),
        token,
      ))
    } else {
      let id = self.locals.len();
      self.locals.insert(name.to_owned(), id);
      Ok(self.ctxvar(id))
    }
  }

  pub fn set_arg(&mut self, name: &str, token: Token) -> Result<CtxVar, CompileError> {
    if self.args.contains_key(name) {
      Err(CompileError::new(
        &format!("argument {} is already defined", name),
        token,
      ))
    } else {
      let id = self.args.len();
      self.args.insert(name.to_owned(), id);
      Ok(CtxVar::Arg(id))
    }
  }
}

#[derive(Debug)]
pub struct CompileError {
  msg: String,
}

impl Display for CompileError {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", self.msg)
  }
}
impl Error for CompileError {}
impl CompileError {
  pub fn new(msg: &str, token: Token) -> Self {
    Self {
      msg: format!(
        "CompileError: {msg} at line={} pos={}",
        token.line, token.pos
      ),
    }
  }
}

pub struct Compiler<'a> {
  src: &'a str,
  start: Option<usize>,
}

impl<'a> Compiler<'a> {
  pub fn new(src: &'a str) -> Self {
    Self { src, start: None }
  }

  fn compile_let(
    &self,
    stmt: &LetStatement,
    ctx: &mut Context,
  ) -> Result<Vec<Instruction>, CompileError> {
    let var = ctx.set(&stmt.ident.name, stmt.ident.token.clone())?;

    let mut code = self.compile_expression(&stmt.expr, ctx)?;
    let instr = match var {
      CtxVar::Local(id) => Instruction::PopLocal(id),
      CtxVar::Global(id) => Instruction::PopGlobal(id),
      _ => unreachable!(),
    };
    code.push(instr);
    Ok(code)
  }

  fn compile_expression(
    &self,
    expr: &Expression,
    ctx: &mut Context,
  ) -> Result<Vec<Instruction>, CompileError> {
    let code = if expr.right.is_none() {
      self.compile_unary_expression(expr, ctx)?
    } else {
      self.compile_binary_expression(expr, ctx)?
    };
    Ok(code)
  }

  fn compile_func_call(
    &self,
    ident: &Identifier,
    args: &Vec<Expression>,
    ctx: &mut Context,
  ) -> Result<Vec<Instruction>, CompileError> {
    let mut code = vec![];
    for expr in args {
      code.extend(self.compile_expression(expr, ctx)?);
    }
    let func_addr = ctx.ensure_func_ident(&ident)?;
    let num_args = args.len();
    code.push(Instruction::Call(func_addr, num_args));
    Ok(code)
  }

  fn compile_left_expression(
    &self,
    expr: &LeftExpression,
    ctx: &mut Context,
  ) -> Result<Vec<Instruction>, CompileError> {
    let code = match expr {
      LeftExpression::Ident(ident) => {
        let var = ctx.ensure(&ident.name, ident.token.clone())?;
        let instr = match var {
          CtxVar::Local(id) => Instruction::PushLocal(id),
          CtxVar::Global(id) => Instruction::PushGlobal(id),
          CtxVar::Arg(id) => Instruction::PushArg(id),
        };
        vec![instr]
      }
      LeftExpression::FuncCall(ident, args) => self.compile_func_call(ident, args, ctx)?,
      LeftExpression::Literal(literal) => match literal {
        Literal::Integer(value, _) => vec![Instruction::PushConstI(*value)],
        Literal::Float(value, _) => vec![Instruction::PushConstF(*value)],
      },
      LeftExpression::Expr(expr) => self.compile_expression(&expr, ctx)?,
    };
    Ok(code)
  }

  fn compile_unary_expression(
    &self,
    expr: &Expression,
    ctx: &mut Context,
  ) -> Result<Vec<Instruction>, CompileError> {
    let mut code = self.compile_left_expression(&expr.left, ctx)?;
    if let Some(op) = &expr.op {
      match op {
        Operator::Plus(_) => {}
        Operator::Minus(_) => code.push(Instruction::Neg),
        Operator::Not(_) => code.push(Instruction::Not),
        _ => {
          // TODO extract token and pass it to the error
          return Err(CompileError {
            msg: "invalid unary operator".into(),
          });
        }
      }
    }
    Ok(code)
  }

  fn compile_binary_expression(
    &self,
    expr: &Expression,
    ctx: &mut Context,
  ) -> Result<Vec<Instruction>, CompileError> {
    let mut code = self.compile_left_expression(&expr.left, ctx)?;
    code.extend(self.compile_expression(expr.right.as_ref().unwrap(), ctx)?);
    let op = expr.op.as_ref().unwrap();
    match op {
      Operator::Plus(_) => code.push(Instruction::AddI),
      Operator::Minus(_) => code.push(Instruction::SubI),
      Operator::Divide(_) => code.push(Instruction::DivI),
      Operator::Multiply(_) => code.push(Instruction::MulI),
      Operator::And(_) => code.push(Instruction::And),
      Operator::Or(_) => code.push(Instruction::Or),
      Operator::Equal(_) => code.push(Instruction::Eq),
      Operator::NotEqual(_) => {
        code.push(Instruction::Eq);
        code.push(Instruction::Not);
      }
      Operator::Less(_) => code.push(Instruction::Less),
      Operator::LessOrEqual(_) => {
        code.push(Instruction::Greater);
        code.push(Instruction::Not);
      }
      Operator::Greater(_) => code.push(Instruction::Greater),
      Operator::GreaterOrEqual(_) => {
        code.push(Instruction::Less);
        code.push(Instruction::Not);
      }
      _ => {
        // TODO extract token and pass it to the error
        return Err(CompileError {
          msg: "invalid unary operator".into(),
        });
      }
    }
    Ok(code)
  }

  fn compile_func_def(
    &mut self,
    stmt: &FuncStatement,
    ip: usize,
    ctx: &mut Context,
  ) -> Result<Vec<Instruction>, CompileError> {
    ctx.set_func(&stmt.ident, ip)?;
    let mut ctx = Context::new(Some(ctx));
    for arg in stmt.args.iter() {
      ctx.set_arg(&arg.name, arg.token.clone())?;
    }
    let body = self.compile_statements(&stmt.body, &mut ctx)?;
    let num_locals = ctx.locals.len();
    let mut code = vec![Instruction::Function(num_locals)];
    code.extend(body);
    Ok(code)
  }

  fn compile_ret(
    &self,
    stmt: &RetStatement,
    ctx: &mut Context,
  ) -> Result<Vec<Instruction>, CompileError> {
    let mut code = vec![];
    if let Some(value) = &stmt.value {
      code.extend(self.compile_expression(value, ctx)?);
    } else {
      code.push(Instruction::PushConstB(false));
    }
    code.push(Instruction::Return);
    Ok(code)
  }

  fn compile_statements(
    &mut self,
    statements: &Vec<Statement>,
    ctx: &mut Context,
  ) -> Result<Vec<Instruction>, CompileError> {
    let mut code = vec![];

    for stmt in statements.iter() {
      let instructions = match stmt {
        Statement::If(_) => todo!(),
        Statement::Let(stmt) => self.compile_let(stmt, ctx)?,
        Statement::Func(stmt) => {
          let ip = code.len() + 1; // reserve space for goto
          let func_code = self.compile_func_def(stmt, ip, ctx)?;
          let goto_ip = ip + func_code.len();
          let mut code = vec![Instruction::Goto(goto_ip)];
          code.extend(func_code);
          code
        }
        Statement::Ret(stmt) => self.compile_ret(stmt, ctx)?,
      };
      code.extend(instructions);
    }
    Ok(code)
  }

  pub fn compile(&mut self, ast: AST) -> Result<Program, CompileError> {
    let mut ctx = Context::default();
    let code = self.compile_statements(&ast.statements, &mut ctx)?;
    Ok(Program { code })
  }
}
