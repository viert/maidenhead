use std::{error::Error, fmt::Display};

use self::{
  basics::{Identifier, Literal, Operator},
  expression::{Expression, LeftExpression},
  statement::{FuncStatement, LetStatement, RetStatement, Statement, AST},
};

use super::lexer::{Token, TokenCursor, TokenType};

pub mod basics;
pub mod expression;
pub mod statement;

#[derive(Debug)]
pub struct ParseError {
  msg: String,
}

impl Display for ParseError {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", self.msg)
  }
}
impl Error for ParseError {}
impl ParseError {
  pub fn new(msg: &str, token: Token) -> Self {
    Self {
      msg: format!("ParseError: {msg} at line={} pos={}", token.line, token.pos),
    }
  }
}

pub struct Parser {
  tc: TokenCursor,
}

impl Parser {
  pub fn new(tc: TokenCursor) -> Self {
    Self { tc }
  }

  fn ensure_token(&mut self) -> Result<Token, ParseError> {
    let token = self.tc.curr();
    if let Some(token) = token {
      Ok(token.clone())
    } else {
      Err(ParseError {
        msg: "runtime error, unexpected end of token stream".into(),
      })
    }
  }

  fn eat(&mut self, token_type: TokenType) -> Result<Token, ParseError> {
    let token = self.ensure_token()?;
    if token.token_type == token_type {
      self.tc.advance();
      Ok(token)
    } else {
      Err(ParseError::new(
        &format!(
          "unexpected token type {}, expected {}",
          token.token_type, token_type
        ),
        token.clone(),
      ))
    }
  }

  fn eat_one_of(&mut self, types: &[TokenType]) -> Result<Token, ParseError> {
    let token = self.ensure_token()?;
    if types.contains(&token.token_type) {
      self.tc.advance();
      Ok(token)
    } else {
      let expected = types
        .iter()
        .map(|t| format!("{t}"))
        .collect::<Vec<String>>()
        .join(", ");
      Err(ParseError::new(
        &format!(
          "unexpected token type {}, expected one of {}",
          token.token_type, expected
        ),
        token.clone(),
      ))
    }
  }

  fn parse_identifier(&mut self, src: &str) -> Result<Identifier, ParseError> {
    let ident = self.eat(TokenType::Identifier)?;
    let literal = ident.literal(src);
    Ok(Identifier {
      token: ident,
      name: literal.to_owned(),
    })
  }

  fn parse_def_args(&mut self, src: &str) -> Result<Vec<Identifier>, ParseError> {
    let mut args = vec![];
    self.eat(TokenType::LeftParenthesis)?;

    let token = self.ensure_token()?;
    if token.token_type == TokenType::RightParenthesis {
      self.tc.advance();
      return Ok(args);
    }

    loop {
      let ident = self.parse_identifier(src)?;
      args.push(ident);
      let token = self.ensure_token()?;
      match token.token_type {
        TokenType::Comma => {
          self.tc.advance();
        }
        TokenType::RightParenthesis => {
          self.tc.advance();
          return Ok(args);
        }
        _ => {
          return Err(ParseError::new(
            &format!("unexpected token {}, expected , or )", token.literal(src)),
            token,
          ));
        }
      }
    }
  }

  fn parse_call_args(&mut self, src: &str) -> Result<Vec<Expression>, ParseError> {
    let mut args = vec![];
    self.eat(TokenType::LeftParenthesis)?;
    let token = self.ensure_token()?;
    if token.token_type == TokenType::RightParenthesis {
      self.tc.advance();
      return Ok(args);
    }

    loop {
      let expr = self.parse_expression(src, None)?;
      args.push(expr);

      let token = self.ensure_token()?;
      match token.token_type {
        TokenType::Comma => {
          self.tc.advance();
        }
        TokenType::RightParenthesis => {
          self.tc.advance();
          return Ok(args);
        }
        _ => {
          return Err(ParseError::new(
            &format!("unexpected token {}, expected , or )", token.literal(src)),
            token,
          ));
        }
      }
    }
  }

  fn parse_left_expression(&mut self, src: &str) -> Result<Option<LeftExpression>, ParseError> {
    let token = self.ensure_token()?;
    match token.token_type {
      TokenType::LeftParenthesis => {
        self.eat(TokenType::LeftParenthesis)?;
        let expr = self.parse_expression(src, None)?;
        self.eat(TokenType::RightParenthesis)?;
        Ok(Some(LeftExpression::Expr(expr)))
      }
      TokenType::Identifier => {
        let ident = Identifier {
          token: token.clone(),
          name: token.literal(src).to_owned(),
        };

        self.tc.advance();
        let token = self.ensure_token()?;
        if token.token_type == TokenType::LeftParenthesis {
          let args = self.parse_call_args(src)?;
          return Ok(Some(LeftExpression::FuncCall(ident, args)));
        }

        Ok(Some(LeftExpression::Ident(ident)))
      }
      TokenType::Integer => {
        let value = token.literal(src).parse::<u64>().unwrap();
        let literal = Literal::Integer(value, token.clone());
        self.tc.advance();
        Ok(Some(LeftExpression::Literal(literal)))
      }
      TokenType::Float => {
        let value = token.literal(src).parse::<f64>().unwrap();
        let literal = Literal::Float(value, token.clone());
        self.tc.advance();
        Ok(Some(LeftExpression::Literal(literal)))
      }
      _ => Ok(None),
    }
  }

  fn parse_expression(
    &mut self,
    src: &str,
    left: Option<LeftExpression>,
  ) -> Result<Expression, ParseError> {
    let left = if left.is_none() {
      self.parse_left_expression(src)?
    } else {
      left
    };

    if let Some(mut left) = left {
      loop {
        let op = parse_operator(&self.ensure_token()?);
        if let Some(op) = op {
          self.tc.advance();
          let right = self.parse_left_expression(src)?;
          if let Some(right) = right {
            let token = self.ensure_token()?;
            let next_op = parse_operator(&token);
            if let Some(next_op) = next_op {
              if op > next_op {
                left = LeftExpression::Expr(Expression {
                  left: Box::new(left),
                  op: Some(op),
                  right: Some(Box::new(right.into())),
                });
                continue;
              } else {
                let right = self.parse_expression(src, Some(right))?;
                return Ok(Expression {
                  left: Box::new(left),
                  op: Some(op),
                  right: Some(Box::new(right)),
                });
              }
            } else {
              return Ok(Expression {
                left: Box::new(left),
                op: Some(op),
                right: Some(Box::new(right.into())),
              });
            }
          } else {
            let token = self.ensure_token()?;
            return Err(ParseError::new(
              &format!(
                "unexpected token {} while parsing expression",
                token.literal(src)
              ),
              token,
            ));
          }
        } else {
          return Ok(Expression {
            left: Box::new(left),
            op: None,
            right: None,
          });
        }
      }
    } else {
      let token = self.ensure_token()?;
      Err(ParseError::new(
        &format!(
          "unexpected token {} while parsing expression",
          token.literal(src)
        ),
        token,
      ))
    }
  }

  fn parse_let(&mut self, src: &str) -> Result<Statement, ParseError> {
    self.eat(TokenType::Keyword)?;
    let ident = self.parse_identifier(src)?;
    self.eat(TokenType::Assign)?;
    let expr = self.parse_expression(src, None)?;
    self.eat(TokenType::Semicolon)?;
    Ok(Statement::Let(LetStatement { ident, expr }))
  }

  fn parse_function(&mut self, src: &str) -> Result<Statement, ParseError> {
    self.eat(TokenType::Keyword)?;
    let ident = self.eat(TokenType::Identifier)?;
    let ident = Identifier {
      name: ident.literal(src).to_owned(),
      token: ident,
    };
    let args = self.parse_def_args(src)?;
    let mut statements = vec![];
    self.eat(TokenType::LeftBrace)?;
    loop {
      let token = self.ensure_token()?;
      if token.token_type == TokenType::RightBrace {
        break;
      }
      let stmt = self.parse_statement(src)?;
      statements.push(stmt);
    }
    self.eat(TokenType::RightBrace)?;
    Ok(Statement::Func(FuncStatement {
      ident,
      args,
      body: statements,
    }))
  }

  fn parse_ret(&mut self, src: &str) -> Result<Statement, ParseError> {
    let ret_token = self.eat(TokenType::Keyword)?;
    let token = self.ensure_token()?;

    let expr = if token.token_type != TokenType::Semicolon {
      Some(self.parse_expression(src, None)?)
    } else {
      None
    };

    self.eat(TokenType::Semicolon)?;
    Ok(Statement::Ret(RetStatement {
      token: ret_token,
      value: expr,
    }))
  }

  fn parse_statement(&mut self, src: &str) -> Result<Statement, ParseError> {
    let token = self.ensure_token()?;
    match token.token_type {
      TokenType::Keyword => {
        let literal = token.literal(src);
        match literal {
          "let" => self.parse_let(src),
          "fn" => self.parse_function(src),
          "ret" => self.parse_ret(src),
          _ => unreachable!(),
        }
      }
      _ => {
        return Err(ParseError::new(
          &format!("unexpected token type {}", token.token_type),
          token.clone(),
        ))
      }
    }
  }

  pub fn parse(&mut self, src: &str) -> Result<AST, ParseError> {
    let mut statements = vec![];
    loop {
      let token = self.ensure_token()?;
      if token.token_type == TokenType::EOF {
        break;
      }
      let stmt = self.parse_statement(src)?;
      statements.push(stmt);
    }
    Ok(AST { statements })
  }
}

fn parse_operator(token: &Token) -> Option<Operator> {
  let token_type = token.token_type.clone();
  match token_type {
    TokenType::And => Some(Operator::And(token.clone())),
    TokenType::Or => Some(Operator::Or(token.clone())),
    TokenType::Plus => Some(Operator::Plus(token.clone())),
    TokenType::Minus => Some(Operator::Minus(token.clone())),
    TokenType::Slash => Some(Operator::Divide(token.clone())),
    TokenType::Asterisk => Some(Operator::Multiply(token.clone())),
    TokenType::Equal => Some(Operator::Equal(token.clone())),
    TokenType::NotEqual => Some(Operator::NotEqual(token.clone())),
    TokenType::Less => Some(Operator::Less(token.clone())),
    TokenType::LessOrEqual => Some(Operator::LessOrEqual(token.clone())),
    TokenType::Greater => Some(Operator::Greater(token.clone())),
    TokenType::GreaterOrEqual => Some(Operator::GreaterOrEqual(token.clone())),
    _ => None,
  }
}
