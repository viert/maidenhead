use crate::lang::lexer::Token;

use super::{basics::Identifier, expression::Expression};

#[derive(Debug)]
pub struct AST {
  pub statements: Vec<Statement>,
}

#[derive(Debug)]
pub enum Statement {
  Let(LetStatement),
  If(IfStatement),
  Func(FuncStatement),
  Ret(RetStatement),
}

#[derive(Debug)]
pub struct LetStatement {
  pub ident: Identifier,
  pub expr: Expression,
}

#[derive(Debug)]
pub struct IfStatement {}

#[derive(Debug)]
pub struct FuncStatement {
  pub ident: Identifier,
  pub args: Vec<Identifier>,
  pub body: Vec<Statement>,
}

#[derive(Debug)]
pub struct RetStatement {
  pub token: Token,
  pub value: Option<Expression>,
}
