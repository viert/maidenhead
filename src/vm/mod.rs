use std::{error::Error, fmt::Display};

use crate::lang::compiler::Program;

#[repr(u8)]
#[derive(Debug, Clone)]
pub enum Instruction {
  PushGlobal(usize),
  PopGlobal(usize),
  PushLocal(usize),
  PopLocal(usize),
  PushArg(usize),
  PushConstI(u64),
  PushConstF(f64),
  PushConstB(bool),
  AddI,
  SubI,
  MulI,
  DivI,
  AddF,
  SubF,
  MulF,
  DivF,
  Not,
  Neg,
  And,
  Or,
  Eq,
  Less,
  Greater,
  Goto(usize),
  GotoIfTrue(usize),
  GotoIfFalse(usize),
  Function(usize),    // num_locals,
  Call(usize, usize), // addr, num_args,
  Return,
  Halt,
}

impl Display for Instruction {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match self {
      Instruction::PushGlobal(v) => write!(f, "push global {v}"),
      Instruction::PopGlobal(v) => write!(f, "pop global {v}"),
      Instruction::PushLocal(v) => write!(f, "push local {v}"),
      Instruction::PopLocal(v) => write!(f, "pop local {v}"),
      Instruction::PushArg(v) => write!(f, "push arg {v}"),
      Instruction::PushConstI(v) => write!(f, "push const int {v}"),
      Instruction::PushConstF(v) => write!(f, "push const float {v}"),
      Instruction::PushConstB(v) => write!(f, "push const bool {v}"),
      Instruction::AddI => write!(f, "add int"),
      Instruction::SubI => write!(f, "sub int"),
      Instruction::MulI => write!(f, "mul int"),
      Instruction::DivI => write!(f, "div int"),
      Instruction::AddF => write!(f, "add float"),
      Instruction::SubF => write!(f, "sub float"),
      Instruction::MulF => write!(f, "mul float"),
      Instruction::DivF => write!(f, "div float"),
      Instruction::Not => write!(f, "not"),
      Instruction::Neg => write!(f, "neg"),
      Instruction::And => write!(f, "and"),
      Instruction::Or => write!(f, "or"),
      Instruction::Eq => write!(f, "eq"),
      Instruction::Less => write!(f, "less"),
      Instruction::Greater => write!(f, "greater"),
      Instruction::Goto(v) => write!(f, "goto {v}"),
      Instruction::GotoIfTrue(v) => write!(f, "goto if true {v}"),
      Instruction::GotoIfFalse(v) => write!(f, "goto if false {v}"),
      Instruction::Function(v) => write!(f, "function {v} locals"),
      Instruction::Call(addr, v) => write!(f, "call {addr} ({v} args)"),
      Instruction::Return => write!(f, "return"),
      Instruction::Halt => write!(f, "halt"),
    }
  }
}

#[derive(Debug)]
pub enum InterpreterError {
  EOP,
  StackOverflow,
  StackCorrupted,
  RuntimeError(String),
}

impl Display for InterpreterError {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match self {
      InterpreterError::EOP => write!(f, "EOP"),
      InterpreterError::StackCorrupted => write!(f, "Stack corrupted"),
      InterpreterError::StackOverflow => write!(f, "Stack overflow"),
      InterpreterError::RuntimeError(msg) => write!(f, "Runtime error: {msg}"),
    }
  }
}

impl Error for InterpreterError {}

#[derive(Default, Debug)]
pub struct Interpreter {
  code: Vec<Instruction>,
  stack: Vec<u64>,
  globals: Vec<u64>,
  ip: usize,
  sp: usize,
  lp: usize,
  ap: usize,
}

impl Interpreter {
  pub fn new(prog: Program, stack_size: usize, num_globals: usize) -> Self {
    let mut stack = vec![];
    stack.resize(stack_size, 0);
    let mut globals = vec![];
    globals.resize(num_globals, 0);

    Self {
      code: prog.code,
      stack,
      globals,
      ip: 0,
      sp: 0,
      lp: 0,
      ap: 0,
    }
  }

  fn push_u64(&mut self, arg: u64) -> Result<(), InterpreterError> {
    if self.sp >= self.stack.len() {
      return Err(InterpreterError::StackOverflow);
    }
    self.stack[self.sp] = arg;
    self.sp += 1;
    Ok(())
  }

  fn push_usize(&mut self, arg: usize) -> Result<(), InterpreterError> {
    self.push_u64(arg as u64)
  }

  fn push_f64(&mut self, arg: f64) -> Result<(), InterpreterError> {
    self.push_u64(ftou(arg))
  }

  fn push_bool(&mut self, arg: bool) -> Result<(), InterpreterError> {
    let v = if arg { 1 } else { 0 };
    self.push_u64(v)
  }

  fn pop_u64(&mut self) -> Result<u64, InterpreterError> {
    if self.sp == 0 {
      return Err(InterpreterError::StackCorrupted);
    }
    self.sp -= 1;
    Ok(self.stack.get(self.sp).unwrap().clone())
  }

  fn pop_f64(&mut self) -> Result<f64, InterpreterError> {
    let v = self.pop_u64()?;
    Ok(utof(v))
  }

  fn pop_bool(&mut self) -> Result<bool, InterpreterError> {
    let v = self.pop_u64()?;
    Ok(v != 0)
  }

  fn pop_usize(&mut self) -> Result<usize, InterpreterError> {
    let v = self.pop_u64()?;
    Ok(v as usize)
  }

  fn jump(&mut self, addr: usize) {
    self.ip = addr
  }

  fn get_global_value(&self, idx: usize) -> Result<u64, InterpreterError> {
    self
      .globals
      .get(idx)
      .cloned()
      .ok_or(InterpreterError::RuntimeError(
        "read from globals out of bounds".into(),
      ))
  }

  fn get_local_value(&self, idx: usize) -> Result<u64, InterpreterError> {
    self
      .stack
      .get(self.lp + idx)
      .cloned()
      .ok_or(InterpreterError::RuntimeError(
        "read from locals out of bounds".into(),
      ))
  }

  fn get_arg_value(&self, idx: usize) -> Result<u64, InterpreterError> {
    self
      .stack
      .get(self.ap + idx)
      .cloned()
      .ok_or(InterpreterError::RuntimeError(
        "read from args out of bounds".into(),
      ))
  }

  fn set_global_value(&mut self, idx: usize, v: u64) -> Result<(), InterpreterError> {
    if idx >= self.globals.len() {
      Err(InterpreterError::RuntimeError(
        "write to globals out of bounds".into(),
      ))
    } else {
      self.globals[idx] = v;
      Ok(())
    }
  }

  fn set_local_value(&mut self, idx: usize, v: u64) -> Result<(), InterpreterError> {
    let idx = self.lp + idx;
    if idx >= self.stack.len() {
      Err(InterpreterError::RuntimeError(
        "write to locals out of bounds".into(),
      ))
    } else {
      self.stack[idx] = v;
      Ok(())
    }
  }

  fn set_return_value(&mut self, v: u64) -> Result<usize, InterpreterError> {
    // sets return value into arg segment
    // returns final sp to be set on return
    if self.ap >= self.stack.len() {
      Err(InterpreterError::RuntimeError(
        "write retvalue out of stack bounds".into(),
      ))
    } else {
      self.stack[self.ap] = v;
      Ok(self.ap + 1)
    }
  }

  pub fn onecmd(&mut self) -> Result<(), InterpreterError> {
    if self.ip >= self.code.len() {
      return Err(InterpreterError::EOP);
    }

    let inst = self.code.get(self.ip).unwrap().clone();

    match inst {
      Instruction::PushGlobal(idx) => self.push_u64(self.get_global_value(idx)?)?,
      Instruction::PopGlobal(idx) => {
        let v = self.pop_u64()?;
        self.set_global_value(idx, v)?;
      }
      Instruction::PushLocal(idx) => self.push_u64(self.get_local_value(idx)?)?,
      Instruction::PopLocal(idx) => {
        let v = self.pop_u64()?;
        self.set_local_value(idx, v)?;
      }
      Instruction::PushArg(idx) => self.push_u64(self.get_arg_value(idx)?)?,
      Instruction::PushConstI(value) => self.push_u64(value)?,
      Instruction::PushConstF(value) => self.push_f64(value)?,
      Instruction::PushConstB(value) => self.push_bool(value)?,
      Instruction::AddI => {
        let b = self.pop_u64()?;
        let a = self.pop_u64()?;
        self.push_u64(a + b)?;
      }
      Instruction::SubI => {
        let b = self.pop_u64()?;
        let a = self.pop_u64()?;
        self.push_u64(a - b)?;
      }
      Instruction::MulI => {
        let b = self.pop_u64()?;
        let a = self.pop_u64()?;
        self.push_u64(a * b)?;
      }
      Instruction::DivI => {
        let b = self.pop_u64()?;
        let a = self.pop_u64()?;
        self.push_u64(a / b)?;
      }
      Instruction::AddF => {
        let b = self.pop_f64()?;
        let a = self.pop_f64()?;
        self.push_f64(a + b)?;
      }
      Instruction::SubF => {
        let b = self.pop_f64()?;
        let a = self.pop_f64()?;
        self.push_f64(a - b)?;
      }
      Instruction::MulF => {
        let b = self.pop_f64()?;
        let a = self.pop_f64()?;
        self.push_f64(a * b)?;
      }
      Instruction::DivF => {
        let b = self.pop_f64()?;
        let a = self.pop_f64()?;
        self.push_f64(a / b)?;
      }
      Instruction::And => {
        let b = self.pop_bool()?;
        let a = self.pop_bool()?;
        self.push_bool(a && b)?;
      }
      Instruction::Or => {
        let b = self.pop_bool()?;
        let a = self.pop_bool()?;
        self.push_bool(a || b)?;
      }
      Instruction::Neg => {
        let v = self.pop_u64()? as i64;
        let v = -v;
        self.push_u64(v as u64)?;
      }
      Instruction::Not => {
        let v = self.pop_bool()?;
        self.push_bool(!v)?;
      }
      Instruction::Eq => {
        let b = self.pop_u64()?;
        let a = self.pop_u64()?;
        self.push_bool(a == b)?;
      }
      Instruction::Less => {
        let b = self.pop_u64()?;
        let a = self.pop_u64()?;
        self.push_bool(a < b)?;
      }
      Instruction::Greater => {
        let b = self.pop_u64()?;
        let a = self.pop_u64()?;
        self.push_bool(a > b)?;
      }
      Instruction::Goto(addr) => {
        self.jump(addr);
        return Ok(());
      }
      Instruction::GotoIfTrue(addr) => {
        if self.pop_bool()? {
          self.jump(addr);
          return Ok(());
        }
      }
      Instruction::GotoIfFalse(addr) => {
        if !self.pop_bool()? {
          self.jump(addr);
          return Ok(());
        }
      }
      Instruction::Function(num_locals) => {
        self.lp = self.sp;
        self.sp += num_locals;
      }
      Instruction::Call(addr, num_args) => {
        let curr_ap = self.ap;
        self.ap = self.sp - num_args; // set new arg ptr
        self.push_usize(self.ip + 1)?; // return address
        self.push_usize(curr_ap)?;
        self.push_usize(self.lp)?;
        self.ip = addr;
        return Ok(());
      }
      Instruction::Return => {
        let retvalue = self.pop_u64()?;
        let retvalue_sp = self.set_return_value(retvalue)?;
        self.sp = self.lp; // clean up func state
        self.lp = self.pop_usize()?;
        self.ap = self.pop_usize()?;
        self.ip = self.pop_usize()?;
        self.sp = retvalue_sp;
        return Ok(());
      }
      Instruction::Halt => {
        self.ip = self.code.len();
        return Ok(());
      }
    }
    self.ip += 1;
    Ok(())
  }

  pub fn run(&mut self) -> Result<(), InterpreterError> {
    loop {
      let res = self.onecmd();
      if let Err(err) = res {
        match err {
          InterpreterError::EOP => {
            return Ok(());
          }
          _ => return Err(err),
        }
      }
    }
  }
}

fn utof(v: u64) -> f64 {
  unsafe {
    let v = (&v as *const u64) as *const f64;
    *v
  }
}

fn ftou(v: f64) -> u64 {
  unsafe {
    let v = (&v as *const f64) as *const u64;
    *v
  }
}

#[cfg(test)]
pub mod tests {
  use super::*;

  #[test]
  fn test_fn_call() {
    let code = vec![
      Instruction::PushConstI(12),
      Instruction::PushConstI(8),
      Instruction::Call(4, 2),
      Instruction::Halt,
      Instruction::Function(0),
      Instruction::PushArg(0),
      Instruction::PushArg(1),
      Instruction::AddI,
      Instruction::Return,
    ];
    let prog = Program { code };
    let mut intr = Interpreter::new(prog, 24, 0);
    let res = intr.run();
    assert!(res.is_ok());
    assert!(intr.sp == 1);
    assert!(intr.stack[0] == 20);
  }
}
