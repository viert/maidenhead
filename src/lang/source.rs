use unicode_segmentation::{Graphemes, UnicodeSegmentation};

#[derive(Debug, Clone, PartialEq)]
pub struct Span(pub usize, pub usize);

pub struct Source<'a> {
  raw: Graphemes<'a>,
  c: Option<&'a str>,
  n: Option<&'a str>,
  idx: usize,
  line: usize,
  pos: usize,
}

impl<'a> Source<'a> {
  pub fn new(raw: &'a str) -> Self {
    let mut raw = raw.graphemes(true);
    let c = raw.next();
    let n = raw.next();
    Self {
      raw,
      c,
      n,
      idx: 0,
      line: 1,
      pos: 1,
    }
  }

  pub fn advance(&mut self) {
    if self.c.is_some() {
      let curr = self.c.unwrap();
      if curr == "\n" {
        self.line += 1;
        self.pos = 1;
      } else {
        self.pos += 1;
      }
      self.idx += curr.len();
      self.c = self.n.take();
      self.n = self.raw.next();
    }
  }

  pub fn curr(&self) -> Option<&'a str> {
    self.c
  }

  pub fn line(&self) -> usize {
    self.line
  }

  pub fn pos(&self) -> usize {
    self.pos
  }

  pub fn idx(&self) -> usize {
    self.idx
  }
}

#[cfg(test)]
pub mod tests {
  use super::*;

  #[test]
  fn test_source() {
    let mut src = Source::new("ab\nc");

    let ch = src.c;
    assert!(ch.is_some());
    assert!(ch.unwrap() == "a");
    assert!(src.line == 1);
    assert!(src.pos == 1);

    src.advance();
    let ch = src.c;
    assert!(ch.is_some());
    assert!(ch.unwrap() == "b");
    assert!(src.line == 1);
    assert!(src.pos == 2);

    src.advance();
    let ch = src.c;
    assert!(ch.is_some());
    assert!(ch.unwrap() == "\n");
    assert!(src.line == 1);
    assert!(src.pos == 3);

    src.advance();
    let ch = src.c;
    assert!(ch.is_some());
    assert!(ch.unwrap() == "c");
    assert!(src.line == 2);
    assert!(src.pos == 1);

    src.advance();
    let ch = src.c;
    assert!(ch.is_none());
  }
}
